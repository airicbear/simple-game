/**
 * Basic 2-dimensional vector.
 * @param {number} x 
 * @param {number} y 
 */
function Vector2(x, y) {
  this.x = x;
  this.y = y;
  this.add = (other) => new Vector2(this.x + other.x, this.y + other.y);
  this.multiply = (n) => new Vector2(this.x * n, this.y * n);
  this.equals = (other) => this.x === other.x && this.y === other.y;
  this.toString = () => "<" + this.x + ", " + this.y + ">";
}

/**
 * Primitive GameObject.
 * @param {string} id 
 * @param {Image} image 
 * @param {Vector2} position 
 * @param {Vector2} scale 
 * @param {number} speed 
 * @param {Vector2} velocity 
 * @param {Collider} collider 
 */
function GameObject(
  id = "GameObject",
  image = null,
  position = new Vector2(0, 0),
  scale = new Vector2(50, 50),
  speed = 1,
  velocity = new Vector2(0, 0),
  collider = new Collider(this)
) {
  this.color = "gray";
  this.id = id;
  this.image = image;
  this.position = position;
  this.scale = scale;
  this.speed = speed;
  this.velocity = velocity;
  this.collider = collider;
  this.setImage = function (imagePath) {
    this.image = document.createElement("img");
    this.image.id = this.id;
    this.image.style = "display: none;";
    this.image.src = imagePath;
    document.body.appendChild(this.image);
  };

  /**
   * Draws the GameObject given its image, position, and scale.
   * @param {CanvasRenderingContext2D} ctx
   */
  this.draw = function (ctx) {
    if (!this.image) {
      ctx.fillStyle = this.color;
      ctx.fillRect(this.position.x, this.position.y, this.scale.x, this.scale.y);
      ctx.fillStyle = "lightgreen";
      ctx.fillText(this.id, this.position.x, this.position.y + 10, this.scale.x);
    } else if (this.image) {
      try {
        ctx.drawImage(this.image, this.position.x, this.position.y, this.scale.x, this.scale.y);
      } catch {
        console.error(this.id, "- Error: Cannot GET", this.image.src);
        this.image = null;
      }
    }
  };

  /**
   * The GameObject's actions to continuously be taken.
   */
  this.update = function () {
    this.position = this.position.add(this.velocity.multiply(this.speed));
  };

  /**
   * Returns a random position with respect to the scene's boundaries.
   * @param {Scene} scene
   */
  this.setRandomPosition = function (scene) {
    let i = 0;
    this.position = new Vector2(Math.random() * (mainScene.width - this.scale.x), Math.random() * (mainScene.height - this.scale.y));
    try {
      while (this.collider.collide(scene.gameObjects[i]) && sceneGameObjects[i] !== this) {
        if (i < scene.gameObjects[i].length) {
          i++;
        } else {
          i = 0;
        }
        this.position = new Vector2(Math.random() * (mainScene.width - this.scale.x), Math.random() * (mainScene.height - this.scale.y));
      }
    } catch {}
  }
}

/**
 * Allows for collisions between GameObjects.
 * @param {GameObject} object 
 */
function Collider(object) {

  /**
   * Returns true if the object is colliding with the other object.
   * @param {GameObject} other
   */
  this.collide = function (other) {
    if (!other || !other.collider) {
      return false;
    }
    let leftBound = object.position.x < other.position.x + other.scale.x;
    let rightBound = object.position.x + object.scale.x > other.position.x;
    let upperBound = object.position.y < other.position.y + other.scale.y;
    let lowerBound = object.position.y + object.scale.y > other.position.y;
    return leftBound && rightBound && upperBound && lowerBound;
  };

  /**
   * Move the object back one step, with respect to it's velocity.
   */
  this.bounce = function () {
    object.position = object.position.add(object.velocity.multiply(-object.speed));
  };
  this.bounceX = function () {
    object.position = object.position.add(new Vector2(object.velocity.multiply(-object.speed).x, 0));
  };
  this.bounceY = function () {
    object.position = object.position.add(new Vector2(0, object.velocity.multiply(-object.speed).y));
  };
}

/**
 * Primitive TextObject.
 * @param {string} text 
 * @param {Vector2} position 
 * @param {Vector2} scale 
 * @param {string} backgroundColor 
 * @param {string} textColor 
 */
function TextObject(
  text = "Text",
  position = new Vector2(2, 10),
  scale = new Vector2(50, 50),
  backgroundColor = 'rgba(0, 0, 0, 0)',
  textColor = 'white'
) {
  this.text = text;
  this.position = position;
  this.scale = scale;
  this.backgroundColor = backgroundColor;
  this.textColor = textColor;

  /**
   * Draw the TextObject's background.
   * @param {CanvasRenderingContext2D} ctx
   */
  this.drawBackground = function (ctx) {
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(this.position.x, this.position.y, this.scale.x, this.scale.y);
  };

  /**
   * Draw the TextObject's text.
   * @param {CanvasRenderingContext2D} ctx
   */
  this.drawText = function (ctx) {
    ctx.fillStyle = textColor;
    ctx.fillText(this.text, this.position.x, this.position.y, this.scale.x);
  };

  /**
   * Draw the TextObject with respect to its text and background color.
   * @param {CanvasRenderingContext2D} ctx
   */
  this.draw = function (ctx) {
    this.drawBackground(ctx);
    this.drawText(ctx);
  };
}

/**
 * A Scene is the environment in which GameObjects interact with each other. Each Scene has a "load" function which returns a HTML5 canvas.
 * @param {number} width 
 * @param {number} height 
 * @param {GameObject[]} gameObjects
 * @param {TextObject[]} textObjects
 * @param {string} backgroundColor
 * @param {number} now
 * @param {number} then
 * @param {number} delta
 * @param {number} interval
 */
function Scene(
  width = 500,
  height = 500,
  gameObjects = [],
  textObjects = [],
  backgroundColor = 'rgba(0, 0, 0, .05)',
  now = Date.now(),
  then = Date.now(),
  delta = this.now - this.then,
  interval = 20
) {
  this.center = (scale = new Vector2(50, 50)) => new Vector2(this.width / 2 - scale.x / 2, this.height / 2 - scale.y / 2);
  this.gameObjects = gameObjects;
  this.width = width;
  this.height = height;
  this.backgroundColor = backgroundColor;
  this.textObjects = textObjects;
  this.now = now;
  this.then = then;
  this.delta = delta;
  this.interval = interval;

  this.outOfBoundsX = function (gameObject) {
    let leftBound = gameObject.position.x < 0;
    let rightBound = gameObject.position.x + gameObject.scale.x > this.width;
    return leftBound || rightBound;
  };

  this.outOfBoundsY = function (gameObject) {
    let upperBound = gameObject.position.y < 0;
    let lowerBound = gameObject.position.y + gameObject.scale.y > this.height;
    return upperBound || lowerBound;
  };

  /**
   * Returns true if the GameObject is out of bounds.
   * @param {GameObject} gameObject
   */
  this.outOfBounds = function (gameObject) {
    return this.outOfBoundsX(gameObject) || this.outOfBoundsY(gameObject);
  };

  /**
   * Return an HTML5 Canvas with the Scene's width and height.
   */
  this.load = function () {
    canvas = document.createElement("canvas");
    canvas.width = this.width;
    canvas.height = this.height;
    return canvas;
  };

  /**
   * Draw a solid-colored background.
   * @param {CanvasRenderingContext2D} ctx
   * @param {string} color
   */
  this.drawBackground = function (ctx, backgroundColor = this.backgroundColor) {
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, this.width, this.height);
  };

  /**
   * Draw and update all objects every frame.
   * @param {CanvasRenderingContext2D} ctx
   */
  this.update = function (ctx) {
    this.drawBackground(ctx);

    for (let i = 0; i < this.gameObjects.length; i++) {
      for (let j = 0; j < this.gameObjects.length; j++) {
        if (this.gameObjects[i] !== this.gameObjects[j] && this.gameObjects[i].collider && this.gameObjects[i].collider.collide(this.gameObjects[j])) {
          this.gameObjects[i].collider.bounce(this.gameObjects[j]);
        }
      }
      this.gameObjects[i].draw(ctx);
      this.gameObjects[i].update();
      if (this.gameObjects[i].collider && this.outOfBoundsX(this.gameObjects[i])) {
        this.gameObjects[i].collider.bounceX();
      }
      if (this.gameObjects[i].collider && this.outOfBoundsY(this.gameObjects[i])) {
        this.gameObjects[i].collider.bounceY();
      }
    }

    for (let i = 0; i < this.textObjects.length; i++) {
      this.textObjects[i].draw(ctx);
    }
  };

  /**
   * Set the frame rate of the game.
   * @param {number} framesPerSecond 
   */
  this.setFps = function (fps = 12) {
    this.then = Date.now();
    this.interval = 1000 / fps;
  };

  /**
   * Continuously loop the game with respect to the frame rate.
   * @param {Scene} scene
   */
  this.gameLoop = function (ctx) {
    window.requestAnimationFrame(() => this.gameLoop(ctx));
    this.now = Date.now();
    this.delta = this.now - this.then;

    if (this.delta > this.interval) {
      this.then = this.now - (this.delta % this.interval);
      this.update(ctx);
    }
  };
}